/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mob.util;

import com.mob.midlet.GuardManager;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Date;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.rms.RecordStore;
import org.kxml.Xml;
import org.kxml.parser.ParseEvent;
import org.kxml.parser.XmlParser;

/**
 *
 * @author hudson
 */
public class SendPending implements Runnable {
    private GuardManager midlet;
    private InputStream is = null;
    private OutputStream os = null;
    private volatile Thread blinker;
    private HttpConnection conn = null;
    private int currentSecond;
    private String txNum,id="",shift,pin,location="",time,name="",confNum="",status = "";
    private boolean exceptionOccured = false;
    
    public SendPending(GuardManager midlet){
        this.midlet = midlet;
    }
    
    public void startConnection(){
        Thread th = new Thread(this);
        blinker = th;
        th.start();
    }
    
    public void stop(){
        blinker = null;
    }
    
    public void setTransaction(String name,String id,String shift,String pin,String location,String trxnNum){
        this.txNum = trxnNum;
        this.name = name;
        this.id = id;
        this.shift = shift;
        this.pin = pin;
        this.location = location;
    }
    
    public void run() {
        //String url = "http://localhost:8080/MarcsWeb/Confirmation";
        String url = "http://www.peakbw.com/cgi-bin/secplus/server2.test";
        byte [] bytes = createTransactionXML(name,id,shift,pin,location,txNum).getBytes();
        ParseEvent pe;
        Thread thisThread = Thread.currentThread();
        while (blinker == thisThread) {//code below is excuted as long as blinker = the current Thread
            currentSecond = Integer.parseInt(((new Date()).toString()).substring(17, 19));
            int stopTime = currentSecond+30;
            status = "";
            while(!status.equalsIgnoreCase("SUCCESSFUL")&&currentSecond<=stopTime){
                try {
                    conn = (HttpConnection)Connector.open(url,Connector.READ_WRITE,true);
                    conn.setRequestMethod(HttpConnection.POST);
                    conn.setRequestProperty("User-Agent","Profile/MIDP-2.1 Confirguration/CLDC-1.1");
                    conn.setRequestProperty("Content-Language", "en-CA");
                    conn.setRequestProperty("Content-Type","text/xml");
                    os = conn.openOutputStream();
                    os.write(bytes, 0, bytes.length);//write transaction to outputstream
                    os.close();
                    if (conn.getResponseCode() == HttpConnection.HTTP_OK){
                        midlet.connectionIsAvailable = true;
                        is = conn.openInputStream();
                        InputStreamReader xmlReader = new InputStreamReader(is);
                        XmlParser parser = new XmlParser( xmlReader );
                        parser.skip();//skips the first line ie <?xml version='1.0' encoding='UTF-8'?>
                        parser.read(Xml.START_TAG, null, "reply");
                        boolean trucking = true;
                        while (trucking) {
                            pe = parser.read();
                            if (pe.getType() == Xml.START_TAG &&pe.getName().equals("transNo")) {
                                pe = parser.read();
                                txNum= pe.getText();
                            }
                            else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("confNo")) {
                                pe = parser.read();
                                confNum = pe.getText();
                            }
                            else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("status")) {
                                pe = parser.read();
                                status= pe.getText();
                            }
                            if (pe.getType() == Xml.END_TAG &&pe.getName().equals("reply"))
                                    trucking = false;
                        }
                        //System.out.println(status);
                        if(status.equalsIgnoreCase("SUCCESSFUL")||"REPEATED SUBMISSION".equals(status)){
                            midlet.activeTrxn.append(name).append("*").append(id).append("*").append(shift).append("*").append(midlet.number).append("*").append(confNum).append("*").append(midlet.getDateTime());
                            RecordStore pendingRecordStore = RecordStore.openRecordStore("pending", true);
                            //delete transaction from pending
                            pendingRecordStore.deleteRecord(midlet.pendingRecordID);
                            pendingRecordStore.closeRecordStore();
                            midlet.connectionIsAvailable = true;
                            //save transaction to history
                            midlet.saveTransaction(midlet.activeTrxn.toString());
                            midlet.connectionIsAvailable = true;
                        }
                    }
                    else{
                        midlet.connectionIsAvailable = false;
                    }
                }
                catch (IOException ex) {
                    midlet.connectionIsAvailable = false;
                    break;
                }
                catch (Exception ex) {
                    break;
                }
                finally{
                    if(is!=null){
                        try {
                            is.close();
                        } catch (IOException ex) {
                        }
                    }
                    if(os!=null){
                        try {
                            os.close();
                        } catch (IOException ex) {
                        }
                    }
                    if(conn!=null){
                        try {
                            conn.close();
                        } catch (IOException ex) {
                        }
                    }
                }
                exceptionOccured = false;
                currentSecond = Integer.parseInt(((new Date()).toString()).substring(17, 19));
            }
            blinker = null;
        }
    }
    
    public String createTransactionXML(String name,String id,String shift,String pin,String location,String txNum){
        time = midlet.getDateTime();
        StringBuffer xmlStr = new StringBuffer();
        xmlStr.append("<?xml version='1.0' encoding='UTF-8'?>");
        xmlStr.append("<record>");
        xmlStr.append("<name>").append(name).append("</name>");
        xmlStr.append("<id>").append(id).append("</id>");
        xmlStr.append("<shift>").append(shift).append("</shift>");
        xmlStr.append("<pin>").append(pin).append("</pin>");
        xmlStr.append("<time>").append(time).append("</time>"); 
        xmlStr.append("<location>").append(location).append("</location>");
        xmlStr.append("<transNo>").append(txNum).append("</transNo>");
        xmlStr.append("</record>");
        System.out.println(xmlStr.toString());
        return xmlStr.toString();
    }
}
