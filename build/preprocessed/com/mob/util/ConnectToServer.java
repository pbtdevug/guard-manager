/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mob.util;

import com.mob.midlet.GuardManager;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Date;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.lcdui.AlertType;
import org.kxml.Xml;
import org.kxml.parser.ParseEvent;
import org.kxml.parser.XmlParser;

/**
 *
 * @author hudson
 */
public class ConnectToServer implements Runnable {
    private GuardManager midlet;
    private InputStream is = null;
    private OutputStream os = null;
    private HttpConnection conn = null;
    private int currentSecond;
    private String txNum,id="",shift,pin,location="",time,name="",confNum="",status = "";
    private boolean exceptionOccured = false;
    
    public ConnectToServer(GuardManager midlet){
        this.midlet = midlet;
    }
    
    public void startConnection(){
        Thread th = new Thread(this);
        th.start();
    }
    
    public void setTransaction(String name,String id,String shift,String pin,String location,String trxnNum){
        this.txNum = trxnNum;
        this.name = name;
        this.id = id;
        this.shift = shift;
        this.pin = pin;
        this.location = location;
    }
    
    public void run() {
        String url = "http://www.askariplus.com/cgi-bin/server2.test.pl";
        //String url = "http://www.peakbw.com/cgi-bin/secplus/server2.test";
        byte [] bytes = createTransactionXML(name,id,shift,pin,location,txNum).getBytes();
        ParseEvent pe;
        status = "";
        int respCode =0;
        
        System.out.println("Conn URL = "+url);
        
        try {
            conn = (HttpConnection)Connector.open(url,Connector.READ_WRITE,true);
            conn.setRequestMethod(HttpConnection.POST);
            conn.setRequestProperty("User-Agent","Profile/MIDP-2.1 Confirguration/CLDC-1.1");
            conn.setRequestProperty("Content-Language", "en-CA");
            conn.setRequestProperty("Content-Type","text/xml");
            os = conn.openOutputStream();
            os.write(bytes, 0, bytes.length);//write transaction to outputstream
            os.close();
            
            try{
                respCode = conn.getResponseCode();
            }
            catch(Exception ex){
                ex.printStackTrace();
            }
            
            System.out.println("Resp Code = "+respCode);
                    
            if (respCode == HttpConnection.HTTP_OK){
                midlet.connectionIsAvailable = true;
                is = conn.openInputStream();

                /*StringBuffer sb = new StringBuffer();
                int ch ;
                while((ch = is.read())!=-1){
                    sb.append((char)ch);
                }
                System.out.println(sb);*/

                InputStreamReader xmlReader = new InputStreamReader(is);
                XmlParser parser = new XmlParser( xmlReader );
                parser.skip();//skips the first line ie <?xml version='1.0' encoding='UTF-8'?>
                parser.read(Xml.START_TAG, null, "reply");
                boolean trucking = true;
                while (trucking) {
                    pe = parser.read();
                    if (pe.getType() == Xml.START_TAG &&pe.getName().equals("transNo")) {
                        pe = parser.read();
                        txNum= pe.getText();
                    }
                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("confNo")) {
                        pe = parser.read();
                        confNum = pe.getText();
                    }
                    else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("status")) {
                        pe = parser.read();
                        status= pe.getText();
                    }
                    if (pe.getType() == Xml.END_TAG &&pe.getName().equals("reply"))
                            trucking = false;
                }
                System.out.println(status);
                if(status.equalsIgnoreCase("SUCCESSFUL")){
                    midlet.historyFlag = false;
                    midlet.connectionIsAvailable = true;
                    if(midlet.activeTrxn.length()>0){
                        midlet.activeTrxn.delete(0, midlet.activeTrxn.length()-1);
                    }
                    midlet.activeTrxn.append(name).append("*").append(id).append("*").append(shift).append("*").append(midlet.number).append("*").append(confNum).append("*").append(midlet.getDateTime());
                    midlet.getAlert().setType(AlertType.CONFIRMATION);
                    midlet.getAlert().setTitle("Successful");
                    midlet.getAlert().setString(status);
                    midlet.createSuccessForm(id,shift,name,midlet.number,confNum,midlet.getDateTime());
                    midlet.switchDisplayable(midlet.getAlert(),midlet.successForm());
                }
                else if("REPEATED SUBMISSION".equals(status)){
                    midlet.connectionIsAvailable = true;
                    midlet.getAlert().setType(AlertType.ERROR);
                    midlet.getAlert().setTitle("Error");
                    midlet.getAlert().setString(status);
                    midlet.switchDisplayable(midlet.getAlert(),midlet.mainMenu());
                }
                else if("INCORRECT PIN".equals(status)){
                    midlet.createIncorrectPinForm(id, shift, name, midlet.number);
                    midlet.connectionIsAvailable = true;
                }
            }
            else{
                midlet.connectionIsAvailable = false;
                exceptionOccured = true;
                midlet.activeTrxn.append(name).append("*").append(id).append("*").append(shift).append("*").append(midlet.number).append("*").append(pin).append("*").append(time).append("*").append(location).append("*").append(txNum);
                midlet.createSuccessForm(id,shift,name,midlet.number,confNum,midlet.getDateTime());
            }
        }
        catch (IOException ex) {
            midlet.connectionIsAvailable = false;
            exceptionOccured = true;
            midlet.activeTrxn.append(name).append("*").append(id).append("*").append(shift).append("*").append(midlet.number).append("*").append(pin).append("*").append(time).append("*").append(location).append("*").append(txNum);
            midlet.createSuccessForm(id,shift,name,midlet.number,confNum,midlet.getDateTime());
        }
        catch (Exception ex) {
            exceptionOccured = true;
            midlet.getAlert().setType(AlertType.ERROR);
            midlet.getAlert().setTitle("Error");
            midlet.getAlert().setString("Application Error");
            midlet.switchDisplayable(midlet.getAlert(),midlet.confirmDetails());
            //ex.printStackTrace();
        }
        finally{
            if(is!=null){
                try {
                    is.close();
                } catch (IOException ex) {
                    midlet.getAlert().setType(AlertType.ERROR);
                    midlet.getAlert().setTitle("Error");
                    midlet.switchDisplayable(midlet.getAlert(),midlet.confirmDetails());
                    midlet.getAlert().setString("hudspon");
                }
            }
            if(os!=null){
                try {
                    os.close();
                } catch (IOException ex) {
                    midlet.getAlert().setType(AlertType.ERROR);
                    midlet.getAlert().setTitle("Error");
                    midlet.switchDisplayable(midlet.getAlert(),midlet.confirmDetails());
                    midlet.getAlert().setString(ex.getMessage());
                }
            }
            if(conn!=null){
                try {
                    conn.close();
                } catch (IOException ex) {
                    midlet.getAlert().setType(AlertType.ERROR);
                    midlet.getAlert().setTitle("Error");
                    midlet.switchDisplayable(midlet.getAlert(),midlet.confirmDetails());
                    midlet.getAlert().setString(ex.getMessage());
                }
            }
        }
        exceptionOccured = false;
        currentSecond = Integer.parseInt(((new Date()).toString()).substring(17, 19));
        
        if(!(status.equalsIgnoreCase("SUCCESSFUL")||status.equalsIgnoreCase("REPEATED SUBMISSION")||"INCORRECT PIN".equals(status))&&exceptionOccured==false){
            //displays alert trying again option
            midlet.switchDisplayable(null, midlet.timeOutAlert());
        }
        //is.close();
    }
    
    public String createTransactionXML(String name,String id,String shift,String pin,String location,String txNum){
        time = midlet.getDateTime();
        StringBuffer xmlStr = new StringBuffer();
        xmlStr.append("<?xml version='1.0' encoding='UTF-8'?>");
        xmlStr.append("<record>");
        xmlStr.append("<name>").append(name).append("</name>");
        xmlStr.append("<id>").append(id).append("</id>");
        xmlStr.append("<shift>").append(shift).append("</shift>");
        xmlStr.append("<pin>").append(pin).append("</pin>");
        xmlStr.append("<time>").append(time).append("</time>"); 
        xmlStr.append("<location>").append(location).append("</location>");
        xmlStr.append("<transNo>").append(txNum).append("</transNo>");
        xmlStr.append("</record>");
        System.out.println(xmlStr.toString());
        return xmlStr.toString();
    }
}
