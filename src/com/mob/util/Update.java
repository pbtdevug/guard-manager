/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mob.util;

import com.mob.midlet.GuardManager;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;
import javax.microedition.io.file.FileConnection;
import javax.microedition.io.file.FileSystemRegistry;
import javax.microedition.rms.RecordEnumeration;
import javax.microedition.rms.RecordStore;
import org.kxml.Xml;
import org.kxml.parser.ParseEvent;
import org.kxml.parser.XmlParser;

/**
 *
 * @author hudson
 */
public class Update implements Runnable {
    private GuardManager midlet;
    private OutputStream os = null;
    private InputStream is = null,fis = null;
    private HttpConnection conn = null;
    private String userId ="0782124567",upSignature = null,updateSign;
    public Vector codes;
    public Hashtable guardHashtable = new Hashtable();
    private boolean guardStoreIsEmpty = true;
    private FileConnection fc = null;
    
    public Update(GuardManager mid){
        this.midlet = mid;
    }
    
    public void startUpdate(){
        this.updateSign = decodeGuardRecords();
        Thread t = new Thread(this);
        t.start();//starts run() or new thread
    }
    
    private String createUpdateXML(String id,String upSign){
        StringBuffer xmlStr = new StringBuffer();
        xmlStr.append("<?xml version='1.0' encoding='UTF-8'?>");
        xmlStr.append("<updateReq>");
        xmlStr.append("<id>").append(id).append("</id>");
        xmlStr.append("<signature>").append(upSign).append("</signature>");
        xmlStr.append("<version>").append(midlet.Version).append("</version>");
        xmlStr.append("</updateReq>");
        //System.out.println(xmlStr.toString());
        return xmlStr.toString();
    }
    
    public void run() {
        String url = "http://www.askariplus.com/cgi-bin/server2.test.pl";
        //String url = "http://www.peakbw.com/cgi-bin/secplus/server2.test";
        System.out.println(url);
        while(upSignature==null){
            try {
                conn = (HttpConnection)Connector.open(url,Connector.READ_WRITE,true);
                conn.setRequestMethod(HttpConnection.POST);
                conn.setRequestProperty("User-Agent","Profile/MIDP-2.1 Confirguration/CLDC-1.1");
                conn.setRequestProperty("Content-Language", "en-CA");
                conn.setRequestProperty("Content-Type","text/xml");
                //open stream connection
                os = conn.openOutputStream();
                byte [] bytes = createUpdateXML(userId,updateSign).getBytes();
                os.write(bytes, 0, bytes.length);
                if(conn.getResponseCode() == HttpConnection.HTTP_OK){//successful connection to server
                    midlet.connectionIsAvailable = true;
                    is= conn.openDataInputStream();
                    viewXML(is);
                }
                else{// can't access server
                    midlet.connectionIsAvailable = false;
                    if(guardStoreIsEmpty==true){
                        loadGuardListFromFileSystem();
                    }
                    break;
                    /*midlet.getAlert().setTitle("Connection Error");
                    midlet.getAlert().setString("Network Unavailable");
                    midlet.switchDisplayable(null,midlet.getAlert());*/
                }
            }
            catch (IOException ex) {
                if(guardStoreIsEmpty==true){
                    loadGuardListFromFileSystem();
                }
                /*midlet.switchDisplayable(null,midlet.getAlert());
                midlet.getAlert().setTitle("I/O Error");
                midlet.getAlert().setString("I/O failure");*/
                midlet.connectionIsAvailable = false;
                break;
            }
            catch (Exception ex) {
                /*midlet.switchDisplayable(null,midlet.getAlert());
                midlet.getAlert().setString(ex.getMessage());*/
                //midlet.connectionIsAvailable = false;
                break;
            }
            finally{
                if(is!=null){
                    try {
                        is.close();
                    } 
                    catch (IOException ex) {
                        /*midlet.switchDisplayable(null,midlet.getAlert());
                        midlet.getAlert().setString(ex.getMessage());*/
                    }
                }
                if(os!=null){
                    try {
                        os.close();
                    } 
                    catch (IOException ex) {
                        /*midlet.switchDisplayable(null,midlet.getAlert());
                        midlet.getAlert().setString(ex.getMessage());*/
                    }
                }
                if(conn!=null){
                    try {
                        conn.close();
                    } 
                    catch (IOException ex) {
                        /*midlet.switchDisplayable(null,midlet.getAlert());
                        midlet.getAlert().setString(ex.getMessage());*/
                    }
                }
                break;
            }
        }
    }
    
    private String decodeGuardRecords(){
        String udateSignature="";
        Vector id_Guard;
        try{
            RecordStore rs = RecordStore.openRecordStore("guardRecords", true);
            if(rs.getNumRecords()>0){//check wether recordStore in not empty
                RecordEnumeration rEnum = rs.enumerateRecords(null, null, false);
                byte [] bts = null;
                while(rEnum.hasNextElement()){
                    bts = rEnum.nextRecord();
                    break;
                }
                String records = new String(bts,0,bts.length);
                udateSignature = GuardManager.split(records, "*").elementAt(0).toString();
                String guardset = GuardManager.split(records, "*").elementAt(1).toString();
                Vector guards = GuardManager.split(guardset.substring(1, guardset.length()-1),",");
                for(int i=0;i<guards.size();i++){
                    id_Guard = GuardManager.split(guards.elementAt(i).toString(),"=");
                    guardHashtable.put((id_Guard.elementAt(0).toString()).trim(), (id_Guard.elementAt(1).toString()).trim());
                    id_Guard.removeAllElements();
                }
                guards.removeAllElements();
                //System.out.println(guardHashtable.toString());
                guardStoreIsEmpty = false;
            }
            else{
                guardStoreIsEmpty = true;
                udateSignature="0";
            }
            rs.closeRecordStore();
        }
        catch(Exception ex){
            /*midlet.getAlert().setType(AlertType.ERROR);
            midlet.getAlert().setTitle("Application Error");
            midlet.getAlert().setString(ex.toString());
            midlet.switchDisplayable(null, midlet.getAlert());
            ex.printStackTrace();*/
        }
        //System.out.println(udateSignature);
        return udateSignature;
    }
    
    private void loadGuardListFromFileSystem(){
        try{
            InputStream strem = getClass().getResourceAsStream("/com/mob/util/guardList.xml");
            viewXML(strem);
            //get available phone file system roots e.g phone memory or memory card
            /*Vector roots = new Vector();
            Enumeration enume = FileSystemRegistry.listRoots();
            while(enume.hasMoreElements()){
                roots.addElement(enume.nextElement());
            }
            //fc = (FileConnection)Connector.open("file:///"+roots.elementAt(roots.size()-1)+"guardList.xml",Connector.READ_WRITE);
            fc.setReadable(true);
            if (fc.exists()){
                fis = fc.openInputStream();
                viewXML(fis);
            }
            else{
                midlet.switchDisplayable(null,midlet.getAlert());
                midlet.getAlert().setString("guardList.xml doesn't exist");
            }*/
        }
        catch(Exception ex){
            //ex.printStackTrace();
        }
    }
    
    private void viewXML(InputStream ips){
        String locaction = new String();
        String action= new String();
        String id = new String();
        String number = new String();
        String gName = new String();
        try{
            InputStreamReader xmlReader = new InputStreamReader(ips);
            XmlParser parser = new XmlParser( xmlReader );
            ParseEvent pe;
            parser.skip();
            parser.read(Xml.START_TAG, null, "updateResp");
        
            boolean trucking = true;
            while (trucking) {
                pe = parser.read();
                if (pe.getType() == Xml.START_TAG &&pe.getName().equals("signature")) {
                    pe = parser.read();
                    upSignature = pe.getText();
                    //System.out.println(upSignature);
                }
                else if (pe.getType() == Xml.START_TAG) {
                    String name = pe.getName();
                    if (name.equals("list")) {
                        while((pe.getType() != Xml.END_TAG) || (pe.getName().equals(name) == false)){
                            pe = parser.read();
                            if (pe.getType() == Xml.START_TAG){
                                String name1 = pe.getName();
                                if (name1.equals("record")) {
                                    while ((pe.getType() != Xml.END_TAG) || (pe.getName().equals(name1) == false)) {
                                        pe = parser.read();
                                        if (pe.getType() == Xml.START_TAG &&pe.getName().equals("name")) {
                                            pe = parser.read();
                                            gName = pe.getText();
                                            System.out.println(gName);
                                        }
                                        else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("number")) {
                                            pe = parser.read();
                                            number = pe.getText();
                                            System.out.println(number);
                                        }
                                        else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("id")) {
                                            pe = parser.read();
                                            id = pe.getText();
                                            System.out.println(id);
                                        }
                                        else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("action")) {
                                            pe = parser.read();
                                            action = pe.getText();
                                            System.out.println(action);
                                        }
                                        else if (pe.getType() == Xml.START_TAG &&pe.getName().equals("location")) {
                                            pe = parser.read();
                                            locaction = pe.getText();
                                            System.out.println(locaction);
                                        }
                                    }   
                                    //hashtable for guards
                                    if("INSERT".equalsIgnoreCase(action)){
                                        guardHashtable.put(id.trim(), gName.trim()+"_"+number.trim()+"_"+locaction.trim());
                                    }
                                    else if("DELETE".equalsIgnoreCase(action)){
                                        guardHashtable.remove(id);
                                    }
                                }
                                else {
                                    while ((pe.getType() != Xml.END_TAG) ||(pe.getName().equals(name1) == false))
                                        pe = parser.read();
                                }
                            }
                        }
                    }
                    else {
                        while ((pe.getType() != Xml.END_TAG) ||(pe.getName().equals(name) == false))
                            pe = parser.read();
                    }
                }
                if (pe.getType() == Xml.END_TAG &&pe.getName().equals("updateResp"))
                    trucking = false;
            }
            //System.out.println(guardHashtable.toString());
            RecordStore.deleteRecordStore("guardRecords");//remove previous records
            byte [] byts = (upSignature+"*"+guardHashtable.toString()).getBytes();//get byte representation
            RecordStore rst = RecordStore.openRecordStore("guardRecords", true);
            rst.addRecord(byts, 0, byts.length);
            rst.closeRecordStore();//save update guard records
        }
        catch (Exception ex) { 
        }
    }
}
