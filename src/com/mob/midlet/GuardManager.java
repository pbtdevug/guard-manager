
package com.mob.midlet;

import com.mob.util.ConnectToServer;
import com.mob.util.SendPending;
import com.mob.util.Update;
import java.util.Date;
import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import javax.microedition.lcdui.*;
import javax.microedition.midlet.*;
import javax.microedition.rms.RecordEnumeration;
import javax.microedition.rms.RecordStore;
import javax.microedition.rms.RecordStoreException;
import org.netbeans.microedition.lcdui.SplashScreen;

/**
 * @author hudson
 */
public class GuardManager extends MIDlet implements CommandListener{
    
//<editor-fold defaultstate="collapsed" desc=" Fields ">
    private boolean midletPaused = false,hisPendFlag = false,pinFlag =false,resetPasswordFlag=false;
    public boolean connectionIsAvailable = false,historyFlag = false,pendingSendFlag =false,runingFirstTime=false;
    private Display display;
    private Command exitCommand;
    private Command cancelCommand;
    private Command backCommand;
    private Command submitCommand;
    private Command okCommand,mainMenuCommand,viewCommand,saveCommand,resetPasswordCommand;
    private Form successForm;
    private Alert alert,timeOutAlert;
    private Form mainMenu;
    private ChoiceGroup categoryChoiceGroup;
    private Form idEntryForm ;
    private ChoiceGroup shiftChoiceGroup;
    private Form confirmDetails;
    private Form waitForm,incorrectPinForm;
    private Form historyPendingForm,passWordForm;
    private ChoiceGroup historyPendingChoiceGroup;
    private SplashScreen splashScreen;
    private Ticker ticker;
    public StringBuffer activeTrxn = new StringBuffer();
    public Vector categ = new Vector();
    private Vector det = new Vector(), recordIDs = new Vector();
    public Hashtable requiredFields = new Hashtable();
    public Update up = new Update(this);
    private ConnectToServer cts = new ConnectToServer(this);
    public SendPending sp = new SendPending(this);
    private String selectedCategory = "",shift,id,location,name,pin,recordID,choiceGroup,pwd;
    public RecordStore pending,history,updateSignature,password;
    public String paymentType,reciept,confNumber="",signature,number;
    public Timer timer,timer1,timer2;
    public int pendingRecordID;
    private Font font;
    private StringItem time;
    private TextField idTextField;
    private TextBox pinTextBox,passwordTextBox;
    public String Version = "v.gm.1.02.20131217.1834";
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" GuardManager ">
    /**
     * The MarcsApp constructor.
     */
    public GuardManager() throws RecordStoreException{
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" initialize ">
    /**
     * Initializes the application. It is called only once when the MIDlet is
     * started. The method is called before the
     * <code>startMIDlet</code> method.
     */
    private void initialize() {
        //pendingSheduler();//start scheduler for pending transactions
        updateSheduler();//start scheduler for updating
        deleteSheduler();// start scheduler for deleting history
        try{//retrieve locally stored password
            password = RecordStore.openRecordStore("password", true);
            if(password.getNumRecords()!=0){
                RecordEnumeration re = password.enumerateRecords(null, null, false);
                byte [] bytes = null;
                while(re.hasNextElement()){
                    bytes = re.nextRecord();// create byte array
                    break;// break b'se there's only one record
                }
                pwd = new String(bytes,0,bytes.length);//convert array into string
            }
        }
        catch(RecordStoreException ex){
            showAlert("Recordstore Error",ex.getMessage());
        }
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" startMIDlet ">
    /**
     * Performs an action assigned to the Mobile Device - MIDlet Started point.
     */
    public void startMIDlet() {
        switchDisplayable(null, getSplashScreen());// display first screen
        up.startUpdate();// start update thread
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" resumeMIDlet ">
    /**
     * Performs an action assigned to the Mobile Device - MIDlet Resumed point.
     */
    public void resumeMIDlet() {
        // write pre-action user code here
 
        // write post-action user code here
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" switchDisplayable ">
    /**
     * Switches a current displayable in a display. The
     * <code>display</code> instance is taken from
     * <code>getDisplay</code> method. This method is used by all actions in the
     * design for switching displayable.
     *
     * @param alert the Alert which is temporarily set to the display;
     * if <code>null</code>, then <code>nextDisplayable</code> is set
     * immediately
     * @param nextDisplayable the Displayable to be set
     */
    public void switchDisplayable(Alert alert, Displayable nextDisplayable) {
        display = getDisplay();
        if (alert == null) {
            display.setCurrent(nextDisplayable);//display intended screen
        } else {
            display.setCurrent(alert, nextDisplayable);//display alert first for some time and the intented screen
        }
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" commandAction for Displayables ">
    /**
     * Called by a system to indicated that a command has been invoked on a
     * particular displayable.
     *
     * @param command the Command that was invoked
     * @param displayable the Displayable where the command was invoked
     */
    public void commandAction(Command command, Displayable displayable) {
        if(command == backCommand){
            if (displayable == successForm) {
                switchDisplayable(null, waitForm());
                successForm.deleteAll();//clear form for next transactions
            }
            else if (displayable == historyPendingForm) {
                switchDisplayable(null, mainMenu());
            }
            else if (displayable == idEntryForm ) {
                switchDisplayable(null, mainMenu());
            }
            else if (displayable == confirmDetails) {
                switchDisplayable(null, idEntryForm());
                confirmDetails.deleteAll();
            }
            else if(displayable == waitForm){
                switchDisplayable(null, confirmDetails());
                waitForm.deleteAll();
            }
            else if(displayable == incorrectPinForm){
                switchDisplayable(null, confirmDetails());
                waitForm.deleteAll();
            }
        }
        else if(command == submitCommand){
            if(displayable == mainMenu){
                mainMenuSubmitAction();
            }
            else if(displayable == idEntryForm){
                createConfirmationForm();
            }
            else if(displayable == confirmDetails){
                switchDisplayable(null, textBox());
            }
        }
        else if(command == cancelCommand){
            if(displayable == waitForm){
                switchDisplayable(null, confirmDetails());
            }
            else{
                switchDisplayable(null, mainMenu());
                resetForms();
            }
        }
        else if(command == exitCommand){
            exitMIDlet(); 
        }
        else if(command == okCommand){
            if(displayable == pinTextBox){
                createWaitForm();
            }
            else if(displayable==passwordTextBox){
                if(pwd.equals(passwordTextBox.getString())){
                    if(resetPasswordFlag == true){
                        switchDisplayable(null, passWordForm());//reset password
                        passwordTextBox.setString("");
                    }
                    else{
                        switchDisplayable(null, mainMenu());// go to main menu
                        passwordTextBox.setString("");
                    }
                }
                else{
                    getAlert().setTitle("Password Error");
                    getAlert().setString("Incorrect Password!");
                    passwordTextBox.setString("");
                    switchDisplayable(getAlert(),passwordTextBox());
                }
            }
            else{
                pendingSendFlag = false;
                cts.startConnection();//restart connection to server after timeout error
                createWaitForm();
            }
        }
        else if(command == SplashScreen.DISMISS_COMMAND){// check wether splashScreen has exited
            try{
                if(password.getNumRecords()==0){//no password saved previously
                    switchDisplayable(null, passWordForm());//enter new password
                    password.closeRecordStore();
                }
                else{//pass exists
                    resetPasswordFlag = false;
                    switchDisplayable(null, passwordTextBox());// login                                            
                    password.closeRecordStore();
                    // write post-action user code here
                }
            }
            catch(RecordStoreException ex){
                showAlert("Recordstore Error",ex.getMessage());
            }
        }
        else if(command == mainMenuCommand){
            switchDisplayable(null, mainMenu()); 
            resetForms();
        }
        else if(command == viewCommand){
            try{
                String storeName;
                //record ID corresponds to the element in recordIDs' vector at selected index of either History or Pending choice groups
                recordID = (recordIDs.elementAt(historyPendingChoiceGroup().getSelectedIndex())).toString();
                choiceGroup = historyPendingChoiceGroup().getLabel();//can be "History" or "Pending"
                if(choiceGroup.equals("History")){
                    storeName = "history";
                }
                else{
                    storeName = "pending";
                }
                decodeTransaction(recordID,storeName);//converts transaction back to string format
            }
            catch(Exception ex){
                showAlert("Error","There is no Transaction selected");
            }
        }
        else if(command == saveCommand){
            //check wether confirm password field and password fields are equal
            if(((TextField)passWordForm.get(0)).getString().equals(((TextField)passWordForm.get(1)).getString())){
                savePassword();
                switchDisplayable(null,mainMenu());
            }
            else{
                showAlert("Password Error","Password Missmatch!");
                ((TextField)passWordForm.get(0)).setString("");//clear password field
                ((TextField)passWordForm.get(1)).setString("");//clear confirm password field
            }
        }
        else if(command == resetPasswordCommand){
            resetPasswordFlag = true;
            switchDisplayable(null,passwordTextBox());
        }
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" exitCommand ">
    /**
     * Returns an initialized instance of exitCommand component.
     *
     * @return the initialized component instance
     */
    public Command getExitCommand() {
        if (exitCommand == null) {
            // write pre-init user code here
            exitCommand = new Command("Exit", Command.EXIT, 0);
            // write post-init user code here
        }
        return exitCommand;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" successForm ">
    /**
     * Returns an initialized instance of successForm component.
     *
     * @return the initialized component instance
     */
    public Form successForm() {
        if (successForm == null) {
            successForm = new Form("Guard Manager", new Item[]{});
            successForm.addCommand(getExitCommand());
            successForm.addCommand(mainMenuCommand());
            successForm.setCommandListener(this);
        }
        return successForm;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" cancelCommand ">
    /**
     * Returns an initialized instance of cancelCommand component.
     *
     * @return the initialized component instance
     */
    public Command cancelCommand() {
        if (cancelCommand == null) {
            // write pre-init user code here
            cancelCommand = new Command("Cancel", Command.CANCEL, 0);
            // write post-init user code here
        }
        return cancelCommand;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" saveCommand ">
    /**
     * Returns an initialized instance of saveCommand component.
     *
     * @return the initialized component instance
     */
    public Command saveCommand() {
        if (saveCommand == null) {
            // write pre-init user code here
            saveCommand = new Command("Save", Command.OK, 0);
            // write post-init user code here
        }
        return saveCommand;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" resetPasswordCommand ">
    /**
     * Returns an initialized instance of resetPasswordCommand component.
     *
     * @return the initialized component instance
     */
    private Command resetPasswordCommand() {
        if (resetPasswordCommand == null) {
            // write pre-init user code here
            resetPasswordCommand = new Command("Reset PWD", Command.OK, 0);
            // write post-init user code here
        }
        return resetPasswordCommand;
    }
//</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc=" backCommand ">
    public Command getBackCommand() {
        if (backCommand == null) {
            backCommand = new Command("Back", Command.OK, 0);
        }
        return backCommand;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" okCommand ">
    public Command okCommand() {
        if (okCommand == null) {
            okCommand = new Command("Ok", Command.OK, 0);
        }
        return okCommand;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" mainMenuCommand ">
    public Command mainMenuCommand() {
        if (mainMenuCommand == null) {
            mainMenuCommand = new Command("Main Menu", Command.OK, 1);
        }
        return mainMenuCommand;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" viewCommand ">
    public Command viewCommand() {
        if (viewCommand == null) {
            viewCommand = new Command("View", Command.BACK, 1);
        }
        return viewCommand;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" submitCommand ">
    /**
     * Returns an initialized instance of itemCommand component.
     *
     * @return the initialized component instance
     */
    public Command submitCommand() {
        if (submitCommand == null) {
            // write pre-init user code here
            submitCommand = new Command("Submit", Command.BACK, 1);
            // write post-init user code here
        }
        return submitCommand;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" mainMenu ">
    /**
     * Returns an initialized instance of mainMenu component.
     *
     * @return the initialized component instance
     */
    public Form mainMenu() {
        if (mainMenu == null) {
            // write pre-init user code here
            mainMenu = new Form("Guard Manager", new Item[]{categoryChoiceGroup()});
            mainMenu.addCommand(getExitCommand());
            mainMenu.addCommand(submitCommand());
            mainMenu.addCommand(resetPasswordCommand());
            mainMenu.setCommandListener(this);
            // write post-init user code here
        }
        return mainMenu;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" timeStringItem ">                                   
    /**
     * Returns an initialized instance of timeStringItem component.
     *
     * @return the initialized component instance
     */
    public StringItem timeStringItem() {
        if (time == null) {                                 
            // write pre-init user code here
            time = new StringItem(getDateTime(), null, Item.PLAIN);  
            time.setFont(getFont());
        }                         
        return time;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" idTextField ">                                   
    /**
     * Returns an initialized instance of idTextField component.
     *
     * @return the initialized component instance
     */
    public TextField idTextField() {
        if (idTextField == null) {                                 
            idTextField = new TextField("Enter Guard ID","", 5, TextField.NUMERIC);
            //idTextField.setPreferredSize(100, 30);
        }                         
        return idTextField;
    }
//</editor-fold>    

//<editor-fold defaultstate="collapsed" desc=" idEntryForm ">
    /**
     * Returns an initialized instance of form component.
     *
     * @return the initialized component instance
     */
    public Form idEntryForm() {
        if (idEntryForm == null) {
            // write pre-init user code here
            idEntryForm  = new Form("Guard Manager", new Item[]{timeStringItem(),shiftChoiceGroup(),idTextField()});
            idEntryForm .addCommand(cancelCommand());
            idEntryForm .addCommand(getBackCommand());
            idEntryForm .addCommand(submitCommand());
            idEntryForm .setCommandListener(this);
            // write post-init user code here
        }
        return idEntryForm ;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" passWordForm ">
    /**
     * Returns an initialized instance of form component.
     *
     * @return the initialized component instance
     */
    public Form passWordForm() {
        if (passWordForm == null) {
            // write pre-init user code here
            passWordForm  = new Form("Save Password", new Item[]{new TextField("Enter Password","g", 20, TextField.PASSWORD),new TextField("Confirm Password","g", 20, TextField.PASSWORD)});
            passWordForm .addCommand(getExitCommand());
            passWordForm .addCommand(saveCommand());
            passWordForm .setCommandListener(this);
            // write post-init user code here
        }
        return passWordForm ;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" confirmDetailsForm ">
    /**
     * Returns an initialized instance of confirmDetails component.
     *
     * @return the initialized component instance
     */
    public Form confirmDetails() {
        if (confirmDetails == null) {
            confirmDetails = new Form("Guard Manager", new Item[]{});
            confirmDetails.addCommand(cancelCommand());
            confirmDetails.addCommand(getBackCommand());
            confirmDetails .addCommand(submitCommand());
            confirmDetails.setCommandListener(this);
        }
        return confirmDetails;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" waitForm ">
    /**
     * Returns an initialized instance of successWaitForm component.
     *
     * @return the initialized component instance
     */
    public Form waitForm() {
        if (waitForm == null) {
            waitForm = new Form("Guard Manager", new Item[]{});
            waitForm.addCommand(cancelCommand());
            waitForm.setCommandListener(this);
        }
        return waitForm;
    }
//</editor-fold> 
    
//<editor-fold defaultstate="collapsed" desc=" incorrectPinForm ">
    /**
     * Returns an initialized instance of incorrectPinForm component.
     *
     * @return the initialized component instance
     */
    public Form incorrectPinForm() {
        if (incorrectPinForm == null) {
            incorrectPinForm = new Form("Guard Manager", new Item[]{});
            incorrectPinForm.addCommand(cancelCommand());
            incorrectPinForm.addCommand(getBackCommand());
            incorrectPinForm.setCommandListener(this);
        }
        return incorrectPinForm;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" categoryChoiceGroup ">
    /**
     * Returns an initialized instance of categoryChoiceGroup component.
     *
     * @return the initialized component instance
     */
    public ChoiceGroup categoryChoiceGroup() {
        if (categoryChoiceGroup == null) {
            // write pre-init user code here
            categoryChoiceGroup = new ChoiceGroup("Main Menu", Choice.EXCLUSIVE);
            categoryChoiceGroup.append("New Submission", null);
            categoryChoiceGroup.append("View History", null);
            categoryChoiceGroup.append("Pending Submissions", null);
            categoryChoiceGroup.setLayout(ImageItem.LAYOUT_DEFAULT);
            // write post-init user code here
        }
        return categoryChoiceGroup;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" shiftChoiceGroup ">
    /**
     * Returns an initialized instance of itemsChoiceGroup component.
     *
     * @return the initialized component instance
     */
    public ChoiceGroup shiftChoiceGroup() {
        if (shiftChoiceGroup == null) {
            shiftChoiceGroup = new ChoiceGroup("Shift", Choice.EXCLUSIVE);
            shiftChoiceGroup.append("DAY", null);
            shiftChoiceGroup.append("NIGHT", null);
            shiftChoiceGroup.append("ABSENT", null);
            shiftChoiceGroup.setLayout(ImageItem.LAYOUT_DEFAULT);
            shiftChoiceGroup.setFont(0, getFont());
            shiftChoiceGroup.setFont(1, getFont());
        }
        return shiftChoiceGroup;
    }
//</editor-fold> 

//<editor-fold defaultstate="collapsed" desc=" historyPendingForm ">
    /**
     * Returns an initialized instance of historyPendingForm component.
     *
     * @return the initialized component instance
     */
    public Form historyPendingForm() {
        if (historyPendingForm == null) {
            historyPendingForm = new Form("history/pending", new Item[]{historyPendingChoiceGroup()});
            historyPendingForm.addCommand(cancelCommand());
            historyPendingForm.addCommand(mainMenuCommand());
            historyPendingForm.addCommand(viewCommand());
            historyPendingForm.setCommandListener(this);
        }
        return historyPendingForm;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" historyPendingChoiceGroup ">
    /**
     * Returns an initialized instance of historyPendingChoiceGroup component.
     *
     * @return the initialized component instance
     */
    public ChoiceGroup historyPendingChoiceGroup() {
        if (historyPendingChoiceGroup == null) {
            // write pre-init user code here
            historyPendingChoiceGroup = new ChoiceGroup("Pending Transactions", Choice.EXCLUSIVE);
            historyPendingChoiceGroup.setLayout(ImageItem.LAYOUT_DEFAULT);
            // write post-init user code here
        }
        return historyPendingChoiceGroup;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" splashScreen ">
    /**
     * Returns an initialized instance of splashScreen component.
     *
     * @return the initialized component instance
     */
    public SplashScreen getSplashScreen() {
        if (splashScreen == null) {
            // write pre-init user code here
            splashScreen = new SplashScreen(getDisplay());
            splashScreen.setTitle("Guard Manager");
            splashScreen.setTicker(getTicker());
            splashScreen.setCommandListener(this);
            splashScreen.setText("");
            splashScreen.setTimeout(5000);
            // write post-init user code here
        }
        return splashScreen;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" ticker ">
    /**
     * Returns an initialized instance of ticker component.
     *
     * @return the initialized component instance
     */
    public Ticker getTicker() {
        if (ticker == null) {
            // write pre-init user code here
            ticker = new Ticker("checking for update");
            // write post-init user code here
        }
        return ticker;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" alert ">
    public Alert getAlert() {
        if (alert == null) {
            alert = new Alert("Error", "error!", null, AlertType.ERROR);
            alert.setTimeout(3000);
        }
        return alert;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" timeOutAlert ">
    public Alert timeOutAlert() {
        if (timeOutAlert == null) {
            timeOutAlert = new Alert("Timeout", "Connection timeout! Do you want to try again?", null, AlertType.INFO);
            timeOutAlert.setTimeout(Alert.FOREVER);
            timeOutAlert.addCommand(okCommand());
            timeOutAlert.addCommand(cancelCommand());
            timeOutAlert.setCommandListener(this);
        }
        return timeOutAlert;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" split">
    public static Vector split(String splitStr, String delimiter) {
        //String [] splitArray;
            StringBuffer token = new StringBuffer();
            Vector tokens = new Vector();

            // split
            char[] chars = splitStr.toCharArray();
            for (int i=0; i < chars.length; i++) {
                if (delimiter.indexOf(chars[i]) != -1) {
                    // we bumbed into a delimiter
                    if (token.length() > 0) {
                        tokens.addElement(token.toString());
                        token.setLength(0);
                    }
                }
                else {
                    token.append(chars[i]);
                }
            }
            // don't forget the "tail"...
            if (token.length() > 0) {
                tokens.addElement(token.toString());
            }
            return tokens;
        }
    //</editor-fold>    

//<editor-fold defaultstate="collapsed" desc=" mainMenuSubmitAction">    
private void mainMenuSubmitAction(){
    try{ 
        selectedCategory = categoryChoiceGroup.getString(categoryChoiceGroup.getSelectedIndex());
        if(selectedCategory.equalsIgnoreCase("New Submission")){
            switchDisplayable(null, idEntryForm());
        }
        else{// if false selectedCategory is either View History or History
            if(selectedCategory.equalsIgnoreCase("View History")){
                historyPendingChoiceGroup().setLabel("History");
                createHistory(); 
            }
            else{
                historyPendingChoiceGroup().setLabel("Pending Transactions");
                createPending();
            }
        }
    }
    catch(IndexOutOfBoundsException ex){
        showAlert("Config Error","configuratins are missing!");
    }
    catch(Exception ex){
        showAlert("Error",ex.getMessage());
    }
}    
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" createConfirmationForm">
    private void createConfirmationForm(){
        confirmDetails().deleteAll();
        pinFlag = false;
            id = ((TextField)idEntryForm().get(2)).getString();
        if(validateField(id)==true){//wether passed validation
            try{
                if(!(up.guardHashtable.get(id)==null)){
                shift = shiftChoiceGroup.getString(shiftChoiceGroup.getSelectedIndex());
                //id returns a string {name_phoneNum_location}
                Vector guardDetails = split(up.guardHashtable.get(id).toString(),"_");//split returned string by _
                number = guardDetails.elementAt(1).toString();
                name = guardDetails.elementAt(0).toString();
                location = guardDetails.elementAt(2).toString();
                confirmDetails().append(new StringItem("Please Confirm Details:\n",null));
                confirmDetails().append(new StringItem(null,getDateTime()+"\n"));
                confirmDetails().append(new StringItem(null,"Guard ID: "+id+"\n"));
                confirmDetails().append(new StringItem(null,shift+" shift\n"));
                confirmDetails().append(new StringItem(null,"Name: "+name+"\n"));
                confirmDetails().append(new StringItem(null,"Number: "+number));
                switchDisplayable(null,confirmDetails());
                }
                else{
                    showAlert("Guard ID Error","Guard with ID: '"+id+"' does not exist!");
                }
            }
            catch(Exception ex){
                showAlert("Error",ex.getMessage());
            }
        } 
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" createIncorrectPinForm">
    public void createIncorrectPinForm(String guardID,String shift,String name,String number){
        try{
            incorrectPinForm().append(new StringItem(null,getDateTime()+"\n"));
            incorrectPinForm().append(new StringItem(null,"Incorrect Pin code, please try again\n"));
            incorrectPinForm().append(new StringItem(null,"Guard ID: "+guardID+"\n"));
            incorrectPinForm().append(new StringItem(null,shift+" shift\n"));
            incorrectPinForm().append(new StringItem(null,"Name: "+name+"\n"));
            incorrectPinForm().append(new StringItem(null,"Number: "+number+"\n"));
            switchDisplayable(null,incorrectPinForm());
        }
        catch(Exception ex){
            showAlert("Error",ex.getMessage());
        }
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" createWaitForm">
    public void createWaitForm(){
        pinFlag = true;
        pin = pinTextBox.getString();
        if(validateField(pin)==true){
            try{
                waitForm().deleteAll();
                waitForm().append(new StringItem("Transaction in progress, please wait ......",null));
                switchDisplayable(null,waitForm());
                //start connection to server {ConnectToServer cts}
                cts.setTransaction(name, id, shift, pin, location, getTrxnNumber());
                cts.startConnection();
            }
            catch(Exception ex){
                showAlert("Recordstore Error",ex.getMessage());
            }
        }
    }    
//</editor-fold>
  
//<editor-fold defaultstate="collapsed" desc=" createSuccessForm">
    public void createSuccessForm(String guardID,String shift,String name,String number,String confNumber,String date){
        //this form is used to view pending, history and successful transactions
        try{
            successForm().deleteAll();
            successForm().append(new StringItem(null,date+"\n"));
            //server connections fail
            if((connectionIsAvailable == false&&hisPendFlag==false)||(hisPendFlag == true&&historyFlag == false)){
                successForm().append(new StringItem(null,"Transaction Incomplete, saved for later submission\n"));
            }
            //server connections successful
            else if(connectionIsAvailable == true&&hisPendFlag==false){
                successForm().append(new StringItem(null,"Transaction Successfull\n"));
                successForm().append(new StringItem(null,"Confirmation Number: "+confNumber+"\n"));
            }
            //coming from pending or history transactions
            else{
                successForm().append(new StringItem(null,"Transaction Complete\n"));
                successForm().append(new StringItem(null,"Confirmation Number: "+confNumber+"\n"));
            }
            //generic information on successForm
            successForm().append(new StringItem(null,"Guard ID: "+guardID+"\n"));
            successForm().append(new StringItem(null,shift+" shift\n"));
            successForm().append(new StringItem(null,"Name: "+name+"\n"));
            successForm().append(new StringItem(null,"Number: "+number+"\n"));
            switchDisplayable(null,successForm());
            //avoid saving transaction when from history or pending
            if(hisPendFlag==false){
                //save after successfuly connectining to server
                saveTransaction(activeTrxn.toString());
            }
        }
        catch(Exception ex){
            showAlert("Error",ex.getMessage());
        }
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" createHistory">
    public void createHistory(){
        //creat history transactin choiceGroup
        RecordEnumeration re;
        try{
            historyPendingChoiceGroup().deleteAll();//delete previous data
            history = RecordStore.openRecordStore("history", true);//open history store
            if(history.getNumRecords()>0){
                historyFlag = true;//flag distinguishes history from pending transactions
                re = history.enumerateRecords(null, null, false);// all history transactions
                //det is a vector that contains each History transactions details
                det.removeAllElements();//deletes previous details
                //recordIDs is a vector that stores all ids for histroy transactions as they occur in the record store 
                recordIDs.removeAllElements();//clear previous ids
                while(re.hasNextElement()){//access each transaction one at a time
                    byte []bytes = re.nextRecord();
                    String record = new String(bytes,0,bytes.length);//represents a single history transaction
                    det = split(record,"*");//history transaction is a string concatinated by * therefore spliting by that
                    //sample record: 1&OCHIENG STAFANO*4802*NIGHT*777777777*1385101940*Fri 22 Nov 2013
                    historyPendingChoiceGroup().append("ID: "+det.elementAt(1)+"-"+det.elementAt(2), null);
                    recordIDs.addElement(split(record,"&").elementAt(0).toString());//add id eg 1
                    //System.out.println(record);
                }
                historyPendingForm().setTitle("Guard Manager");
            }
            else{
                //historyPendingForm().removeCommand(viewCommand());
            }
            history.closeRecordStore();
            switchDisplayable(null,historyPendingForm());
        }
        catch(IndexOutOfBoundsException ex){
            showAlert("History Empty","No history!");
        }
        catch(RecordStoreException ex){
            showAlert("Recordstore Error",ex.getMessage());
        }                
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" createPending">
    public void createPending(){
        //creates pending choice group. Flow same as creatHistroy()
        try{
            historyPendingChoiceGroup().deleteAll();
            pending = RecordStore.openRecordStore("pending", true);
            if(pending.getNumRecords()>0){
                historyFlag = false;
                RecordEnumeration re = pending.enumerateRecords(null, null, false);
                det.removeAllElements();
                recordIDs.removeAllElements();
                while(re.hasNextElement()){
                    byte []bytes = re.nextRecord();
                    String record = new String(bytes,0,bytes.length);
                    det = split((split(record,"&").elementAt(1).toString()),"*");
                    historyPendingChoiceGroup().append("ID: "+det.elementAt(1)+"-"+det.elementAt(2), null);
                    recordIDs.addElement(split(record,"&").elementAt(0).toString());
                }
                historyPendingForm().setTitle("Guard Manager");
            }
            pending.closeRecordStore();
            switchDisplayable(null,historyPendingForm());
        }
        catch(IndexOutOfBoundsException ex){
            showAlert("Zero Transactions","No pending Transactions!");
        }
        catch(RecordStoreException ex){
            showAlert("Recordstore Error",ex.getMessage());
        }
    }
//</editor-fold>    
    
//<editor-fold defaultstate="collapsed" desc=" savePassword">
    private void savePassword(){
        //convert contents of passfield into bytes
        byte[] bytes = ((TextField)passWordForm.get(1)).getString().getBytes();
        try{
            String [] recordstores = RecordStore.listRecordStores();//lists all available record stores
            for(int x = 0; x<recordstores.length; x++){
                if("password".equals(recordstores[x])){//check if password exists
                    //delete currently existing password
                    RecordStore.deleteRecordStore(recordstores[x]);
                    break;
                }
            }
            //save new user details
            RecordStore rs3 = RecordStore.openRecordStore("password", true);
            rs3.addRecord(bytes, 0, bytes.length);
            rs3.closeRecordStore();
        }
        catch(RecordStoreException ex){
            showAlert("Recordstore Error",ex.getMessage());
        }   
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" getTrxnNumber">    
    private String getTrxnNumber() {
        String mnt = "";
        String [] months = {"Jan","Feb","Mar", "Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct","Nov", "Dec"};
        Vector dateInfo = split((new Date()).toString()," ");//Fri Nov 29 11:57:35 EAT 2013
        if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[0])){
            mnt = "01";
        }
        else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[1])){
            mnt = "02";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[2])){
            mnt = "03";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[3])){
            mnt = "04";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[4])){
            mnt = "05";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[5])){
            mnt = "06";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[6])){
            mnt = "07";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[7])){
            mnt = "08";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[8])){
            mnt = "09";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[9])){
            mnt = "10";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[10])){
            mnt = "11";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[11])){
            mnt = "12";
        }
        String txn = dateInfo.elementAt(2)+mnt+(dateInfo.elementAt(5).toString().substring(2, 4))+(dateInfo.elementAt(3).toString().substring(0, 2))+(dateInfo.elementAt(3).toString().substring(3, 5))+(dateInfo.elementAt(3).toString().substring(6, 8));
        return txn;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" getDateTime">    
    public String getDateTime() {
        String mnt = "";
        String [] months = {"Jan","Feb","Mar", "Apr", "May", "Jun", "Jul","Aug", "Sep", "Oct","Nov", "Dec"};
        Vector dateInfo = split((new Date()).toString()," ");
        if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[0])){
            mnt = "01";
        }
        else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[1])){
            mnt = "02";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[2])){
            mnt = "03";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[3])){
            mnt = "04";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[4])){
            mnt = "05";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[5])){
            mnt = "06";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[6])){
            mnt = "07";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[7])){
            mnt = "08";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[8])){
            mnt = "09";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[9])){
            mnt = "10";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[10])){
            mnt = "11";
        }else if(dateInfo.elementAt(1).toString().equalsIgnoreCase(months[11])){
            mnt = "12";
        }
        String txn = dateInfo.elementAt(0).toString()+" "+dateInfo.elementAt(2).toString()+" "+dateInfo.elementAt(1)+" "+dateInfo.elementAt(5).toString();
        return txn;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" saveTransaction"> 
    public void saveTransaction(String activeTrxn){
        //activeTrxn: MBONYE PHILIP*9990*DAY*781191914*1385714799*Fri 29 Nov 2013
        try{
            RecordStore rst;
            if(connectionIsAvailable==false){
                rst = RecordStore.openRecordStore("pending", true);
            }
            else{
                rst = RecordStore.openRecordStore("history", true);
            }
            int idt = rst.getNextRecordID();
            String transaction = idt+"&"+activeTrxn;//16&MBONYE PHILIP*9990*DAY*781191914*1385714799*Fri 29 Nov 2013                                                                              
            byte [] bytes1 = transaction.getBytes();
            rst.addRecord(bytes1, 0, bytes1.length);
            rst.closeRecordStore();
            //System.out.println(transaction);
        }
        catch(RecordStoreException ex){
            showAlert("Recordstore Error",ex.getMessage());
        }
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" decodeTransaction">
public void decodeTransaction(String recordID,String recordStoreName){ // creates back transaction saved as history from RecordSore object to string object
    hisPendFlag = true;
    //historyFlag = true;
    try{
        RecordStore rs = RecordStore.openRecordStore(recordStoreName, true);
        byte []bytes = rs.getRecord(Integer.parseInt(recordID));
        String record = new String(bytes,0,bytes.length);
        //System.out.println(record);
        Vector trxn = split(split(record,"&").elementAt(1).toString(),"*");//16&MBONYE PHILIP*9990*DAY*781191914*1385714799*Fri 29 Nov 2013
        //if(historyFlag == true){
            createSuccessForm(trxn.elementAt(1).toString(),trxn.elementAt(2).toString(),trxn.elementAt(0).toString(),trxn.elementAt(3).toString(),trxn.elementAt(4).toString(),trxn.elementAt(5).toString());
        /*}
        else{
            createSuccessForm(trxn.elementAt(1).toString(),trxn.elementAt(2).toString(),trxn.elementAt(0).toString(),trxn.elementAt(3).toString(),trxn.elementAt(4).toString(),trxn.elementAt(5).toString());
        }*/
        successForm().setTitle("Guard Manager");
        rs.closeRecordStore();
    }
    catch(RecordStoreException ex){
        showAlert("Recordstore Error",ex.getMessage());
    }
} 
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" resetForms">    
    private void resetForms(){
        try{
            if(!(confirmDetails == null)){
                confirmDetails.deleteAll();
            }
            if(!(historyPendingForm == null)){
                //historyPendingForm.deleteAll();
            }
            if(!(successForm == null)){
                successForm.deleteAll();
            }
            if(!(idEntryForm  == null)){
                //idEntryForm.deleteAll();
            }
            if(!(waitForm == null)){
                waitForm.deleteAll();
            }
            //successWaitForm = null;
        }
        catch(Exception ex){
            showAlert("Error",ex.toString());
        }
    }
//</editor-fold>
  
//<editor-fold defaultstate="collapsed" desc=" validateField">    
    private boolean validateField(String field){
        boolean passes = false;
        int fieldLength = field.length();
        try{
            if(((fieldLength<=4&&fieldLength>0)&&pinFlag==false)||(fieldLength==4&&pinFlag==true)){
                passes = true;
            }
            else{
                if(pinFlag==false){
                    showAlert("Guard ID Error","Guard ID must be 1-4 characters long!");
                }
                else{
                    showAlert("Pin code Error","pin code must be 4 characters long!");
                }
                passes = false;
            }
        }
        catch(Exception ex){
            showAlert("Error",ex.toString());
        }
        return passes;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" font ">
    /**
     * Returns an initialized instance of font component.
     *
     * @return the initialized component instance
     */
    public Font getFont() {
        if (font == null) {
            // write pre-init user code here
            font = Font.getFont(Font.FACE_SYSTEM, Font.STYLE_PLAIN, Font.SIZE_SMALL);
            // write post-init user code here
        }
        return font;
    }
    //</editor-fold>

//<editor-fold defaultstate="collapsed" desc=" textBox ">
    public TextBox textBox() {
        if (pinTextBox == null) {
            pinTextBox = new TextBox("Enter a 4 digit Pin", null, 5, TextField.NUMERIC);
            pinTextBox.addCommand(okCommand());
            pinTextBox.addCommand(cancelCommand());
            pinTextBox.setCommandListener(this);
        }
        return pinTextBox;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" passwordTextBox ">
    public TextBox passwordTextBox() {
        if (passwordTextBox == null) {
            passwordTextBox = new TextBox("Enter Password", null, 20, TextField.PASSWORD);
            passwordTextBox.addCommand(okCommand());
            passwordTextBox.addCommand(getExitCommand());
            passwordTextBox.setCommandListener(this);
        }
        return passwordTextBox;
    }
//</editor-fold>
 
//<editor-fold defaultstate="collapsed" desc=" showAlert ">
    public void showAlert(String title,String msg){
        getAlert().setString(msg);
        getAlert().setTitle(title);
        switchDisplayable(null,getAlert());
    }
//</editor-fold>  
    
//<editor-fold defaultstate="collapsed" desc=" getDisplay">
    public Display getDisplay() {
        return Display.getDisplay(this);
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" exitMIDlet">
    /**
     * Exits MIDlet.
     */
    public void exitMIDlet() {
        //kill all background threads first 
        timer.cancel();
        timer1.cancel();
        timer2.cancel();
        switchDisplayable(null, null);
        destroyApp(true);
        notifyDestroyed();
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" startApp">
    /**
     * Called when MIDlet is started. Checks whether the MIDlet have been
     * already started and initialize/starts or resumes the MIDlet.
     */
    public void startApp() {
        if (midletPaused) {
            resumeMIDlet();
        } else {
            initialize();
            startMIDlet();
        }
        midletPaused = false;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" pauseApp">    
    /**
     * Called when MIDlet is paused.
     */
    public void pauseApp() {
        midletPaused = true;
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" destroyApp">    
    /**
     * Called to signal the MIDlet to terminate.
     *
     * @param unconditional if true, then the MIDlet has to be unconditionally
     * terminated and all resources has to be released.
     */
    public void destroyApp(boolean unconditional) {
    }
//</editor-fold>
  
//<editor-fold defaultstate="collapsed" desc=" SendPendingTask"> 
    class  SendPendingTask  extends TimerTask{
        public void run() {
            //action to be performed
            try{
                RecordStore pendingRecordStore = RecordStore.openRecordStore("pending", true);
                //when there are pending transactions
                if(!(pendingRecordStore.getNumRecords()==0)){
                    RecordEnumeration re = pendingRecordStore.enumerateRecords(null, null, false);
                    byte [] bytes = null;
                    while(re.hasNextElement()){
                        bytes = re.nextRecord();
                        break;
                    }
                    String trxn = new String(bytes,0,bytes.length);
                    Vector pendingTrxn = split(trxn,"&");//16&MBONYE PHILIP*9990*DAY*781191914*1385714799*Fri 29 Nov 2013
                    pendingRecordID = Integer.parseInt(pendingTrxn.elementAt(0).toString());
                    Vector trans = split(pendingTrxn.elementAt(1).toString(),"*");//MBONYE PHILIP*9990*DAY*781191914*1385714799*Fri 29 Nov 2013
                    sp.setTransaction(trans.elementAt(0).toString(), trans.elementAt(1).toString(), trans.elementAt(2).toString(), trans.elementAt(4).toString(), trans.elementAt(6).toString(), trans.elementAt(7).toString());
                    sp.startConnection();
                }
                pendingRecordStore.closeRecordStore();
                //timer.cancel();
            }
            catch(Exception ex){
            }
        }
    
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" pendingSheduler">    
    private void pendingSheduler() {
        timer = new Timer();
        try{
            //thread wakes up 1 min (60000) after the start of app to start sending pending transactions 
            timer.scheduleAtFixedRate(new GuardManager.SendPendingTask(), 60000,1*120000);
            // every after 2 min (120000) one pending transaction is sent to the server until they are all done
        }
        catch(IllegalStateException ex){
        }
        catch(IllegalArgumentException ex){
        }
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" updateTask">
    class  UpdateTask  extends TimerTask{

        public void run() {
            up.startUpdate();// start update thread
        }
        
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" updateSheduler">
    private void updateSheduler() {
        timer1 = new Timer();
        try{
            //wakes up 5 min after the start of app and updates every after 5 min
            timer1.scheduleAtFixedRate(new GuardManager.UpdateTask(), 300000,1*300000);
        }
        catch(IllegalStateException ex){
        }
        catch(IllegalArgumentException ex){
        }
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" historyDeleteTask">
    class  HistoryDeleteTask  extends TimerTask{

        public void run() {
            try{
                RecordStore history = RecordStore.openRecordStore("history", true);
                if(!(history.getNumRecords()==0)){
                    RecordEnumeration re = history.enumerateRecords(null, null, false);
                    byte [] bytes;
                    Vector historyTrxn = new Vector();
                    String datetime;
                    String trxn;
                    String dat;
                    int date,currentDate,recordid;
                    while(re.hasNextElement()){
                        historyTrxn.removeAllElements();
                        bytes = re.nextRecord();
                        trxn = new String(bytes,0,bytes.length);
                        //[MBONYE PHILIP, 9990, DAY, 781191914, 1387291520, Tue 17 Dec 2013]
                        historyTrxn = split((split(trxn,"&").elementAt(1).toString()),"*");
                        recordid = Integer.parseInt((split(trxn,"&").elementAt(0).toString()));
                        //System.out.println(historyTrxn);
                        //System.out.println(getDateTime());
                        datetime = historyTrxn.elementAt(historyTrxn.size()-1).toString();
                        //System.out.println(datetime);
                        dat = (split(datetime," ").elementAt(1).toString());
                        //prevMonth = (split(datetime," ").elementAt(2).toString());
                        date = Integer.parseInt(dat);
                        //System.out.println("prev="+date);
                        //Tue 17 Dec 2013
                        currentDate = Integer.parseInt((split((new Date()).toString()," ").elementAt(2).toString()));
                        //currMonth = (split((new Date()).toString()," ").elementAt(1).toString());
                        //System.out.println("curr="+currentDate);
                        if(date<currentDate||(date>currentDate&&currentDate==1)){
                            history.deleteRecord(recordid);
                        }
                         //System.out.println(hr+currentHour);
                    }
                }
            }
            catch(Exception ex){
                
            }
            //timer2.cancel();
        }
        
    }
//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc=" deleteSheduler">
    private void deleteSheduler() {
        timer2 = new Timer();
        try{
            timer2.scheduleAtFixedRate(new GuardManager.HistoryDeleteTask(), 60000,1*30000);
        }
        catch(IllegalStateException ex){
        }
        catch(IllegalArgumentException ex){
        }
    }
//</editor-fold>
    
}

